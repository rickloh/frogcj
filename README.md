Open source devmap mod 'frogcj'.
Licensed under LGPL 3.0 - so if you fix/improve frogcj or add anything to it, it is required to make your changes open source.
You don't have to open source your own custom files, but it is still recommended.

- Ridgepig