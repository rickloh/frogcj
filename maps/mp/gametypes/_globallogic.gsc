/*
 * Very stripped down basic version of the regular _globallogic.
 * We barely need anything done in here, since most damage sources, weapons, scores etc. are all disabled
 */

#include maps\mp\_utility;
#include maps\mp\gametypes\_hud_util;
#include common_scripts\utility;

init()
{
    // Most maps don't have a _tweakables.gsc file
    if ( !isDefined( level.tweakablesInitialized ) )
        maps\mp\gametypes\_tweakables::init();
    
    // Ensure certain dvars are never changed during gameplay
    maps\mp\gametypes\frogcj\_frogcj::setServerDvars();

    // We're not console plebs
    level.splitscreen = false;
    level.console     = false;
    level.rankedMatch = false;
    
    level.onlineGame  = true;

    // Set up the correct script file name for current map and check gametype
    level.script      = toLower(getDvar("mapname"));
    level.gametype    = "cj"; // No other gametypes allowed

    level.otherTeam["allies"] = "axis";
    level.otherTeam["axis"] = "allies";
    
    // CJ is based on FFA
    level.teamBased = false;
    
    level.overrideTeamScore = false;
	level.overridePlayerScore = false;
	level.displayHalftimeText = false;
    level.displayRoundEndText = true;
    
    // No score limit
    level.endGameOnScoreLimit = false;
    
    level.endGameOnTimeLimit = true;
    
    // TODO: Can probably be removed
    level.postRoundTime = 8.0;

    level.dropTeam = getdvarint("sv_maxclients");
    
    // Keep track of players in the server
    level.players = [];
    
    // We ain't scrubs
    level.oldschool = false;
    
    // TODO: Do we use these models and shaders in CJ?
    precacheModel( "vehicle_mig29_desert" );
    precacheModel( "projectile_cbu97_clusterbomb" );
    precacheModel( "tag_origin" );    

    precacheShader( "faction_128_usmc" );
    precacheShader( "faction_128_arab" );
    precacheShader( "faction_128_ussr" );
    precacheShader( "faction_128_sas" );
    
    level.fx_airstrike_afterburner = loadfx ("fire/jet_afterburner");
    level.fx_airstrike_contrail = loadfx ("smoke/jet_contrail");
    
    // Initialize the CJ mod
    maps\mp\gametypes\frogcj\_frogcj::init();
}

SetupCallbacks()
{
    // TODO: Probably can remove most of these, but can't be bothered in case we ever need to maintain scores
    level.spawnPlayer = ::spawnPlayer;
    level.spawnClient = ::spawnClient;
    level.spawnSpectator = ::spawnSpectator;
    level.spawnIntermission = ::spawnIntermission;
    level.onPlayerScore = ::blank;
    level.onTeamScore = ::blank;
    
    level.onXPEvent = ::blank;
    level.waveSpawnTimer = ::blank;
    
    level.onSpawnPlayer = ::blank;
    level.onSpawnSpectator = ::default_onSpawnSpectator;
    level.onSpawnIntermission = ::default_onSpawnIntermission;
    level.onRespawnDelay = ::blank;
    
	level.onForfeit = ::blank;
    level.onTimeLimit = ::default_onTimeLimit;
    level.onScoreLimit = ::blank;
    level.onDeadEvent = ::blank;
    level.onOneLeftEvent = ::blank;
    level.giveTeamScore = ::blank;
    level.givePlayerScore = ::blank;
    
    level.onPrecacheGametype = ::blank;
    level.onStartGameType = ::blank;
    level.onPlayerConnect = ::blank;
    level.onPlayerDisconnect = ::blank;
    level.onPlayerDamage = ::blank;
    level.onPlayerKilled = ::blank;

    level.onEndGame = ::blank;

    level.autoassign = ::menuStartPlaying;
    level.spectator = ::menuSpectator;
    level.class = ::blank;
    level.allies = ::menuStartPlaying;
    level.axis = ::blank;
}

// to be used with things that are slow.
// unfortunately, it can only be used with things that aren't time critical.
WaitTillSlowProcessAllowed()
{
	// wait only a few frames if necessary
	// if we wait too long, we might get too many threads at once and run out of variables
	// i'm trying to avoid using a loop because i don't want any extra variables
	if ( level.lastSlowProcessFrame == gettime() )
	{
		wait .05;
		if ( level.lastSlowProcessFrame == gettime() )
		{
		wait .05;
			if ( level.lastSlowProcessFrame == gettime() )
			{
				wait .05;
				if ( level.lastSlowProcessFrame == gettime() )
				{
					wait .05;
				}
			}
		}
	}
	
	level.lastSlowProcessFrame = gettime();
}

leaderDialog( dialog, team, group, excludeList )
{
    // Just so other script files don't error
}

leaderDialogOnPlayer( dialog, group )
{
    // Just so other script files don't error
}

updateObjectiveText()
{
    // Just so other script files don't error
}

Callback_PlayerLastStand( eInflictor, attacker, iDamage, sMeansOfDeath, sWeapon, vDir, sHitLoc, psOffsetTime, deathAnimDuration )
{
    // Just so other script files don't error
}

setObjectiveText( team, text )
{
    // Just so other script files don't error
}

setObjectiveScoreText( team, text )
{
    // Just so other script files don't error
}

setObjectiveHintText( team, text )
{
    // Just so other script files don't error
}

isValidClass( class )
{
	return isdefined( class ) && class != "";
}

// Blank. That's right
blank(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10)
{
    
}

// The only way our CJ game will end: time limit
default_onTimeLimit()
{
    thread endGame( game["strings"]["time_limit_reached"] );
}

// Not practical, but whatever let's keep it
forceEnd()
{
    if ( level.hostForcedEnd || level.forcedEnd )
        return;
    
    level.forcedEnd = true;
    level.hostForcedEnd = true;
    
    endString = &"MP_HOST_ENDED_GAME";
    
    makeDvarServerInfo( "ui_text_endreason", endString );
    setDvar( "ui_text_endreason", endString );
    thread endGame( endString );
}

spawnPlayer()
{
    self endon("disconnect");
    self endon("joined_spectators");
    self notify("spawned");
    self notify("end_respawn");

    self setSpawnVariables();

    self.sessionteam = "none";

    hadSpawned = self.hasSpawned;

    self.sessionstate = "playing";
    self.spectatorclient = -1;
    self.killcamentity = -1;
    self.archivetime = 0;
    self.psoffsettime = 0;
    self.statusicon = "";
    self.maxhealth = getDvarInt( 100 ); // This seems enough
    self.health = self.maxhealth;
    self.friendlydamage = undefined;
    self.hasSpawned = true;
    self.spawnTime = getTime();
    self.afk = false;
    self.lastStand = undefined;
    
    self freezeControls( false );
    self enableWeapons();
    
    // Finally, actually spawn the player
    [[level.onSpawnPlayer]]();
    
    waittillframeend;
    
    self notify( "spawned_player" );

    self logstring( "S " + self.origin[0] + " " + self.origin[1] + " " + self.origin[2] );
    
    // Might as well place this here
    visionSetNaked( getDvar( "mapname" ), 0 );
    
    if ( game["state"] == "postgame" )
    {
        assert( !level.intermission );
        // We're in the victory screen, but before intermission
        self freezePlayerForRoundEnd();
    }
}

spawnSpectator( origin, angles )
{
    self notify("spawned");
    self notify("end_respawn");
    in_spawnSpectator( origin, angles );
}

// spawnSpectator clone without notifies for spawning between respawn delays
respawn_asSpectator( origin, angles )
{
    in_spawnSpectator( origin, angles );
}

// spawnSpectator helper
in_spawnSpectator( origin, angles )
{
    self setSpawnVariables();
    
    // don't clear lower message if not actually a spectator,
    // because it probably has important information like when we'll spawn
    if ( self.pers["team"] == "spectator" )
        self clearLowerMessage();
    
    self.sessionstate = "spectator";
    self.spectatorclient = -1;
    self.killcamentity = -1;
    self.archivetime = 0;
    self.psoffsettime = 0;
    self.friendlydamage = undefined;

    if(self.pers["team"] == "spectator")
        self.statusicon = "";
    else
        self.statusicon = "hud_status_dead";

    maps\mp\gametypes\_spectating::setSpectatePermissions();

    [[level.onSpawnSpectator]]( origin, angles );
}

getPlayerFromClientNum( clientNum )
{
    if ( clientNum < 0 )
        return undefined;
    
    for ( i = 0; i < level.players.size; i++ )
    {
        if ( level.players[i] getEntityNumber() == clientNum )
            return level.players[i];
    }
    return undefined;
}

default_onSpawnSpectator( origin, angles)
{
    if( isDefined( origin ) && isDefined( angles ) )
    {
        self spawn(origin, angles);
        return;
    }
    
    spawnpointname = "mp_global_intermission";
    spawnpoints = getentarray(spawnpointname, "classname");
    assert( spawnpoints.size );
    spawnpoint = maps\mp\gametypes\_spawnlogic::getSpawnpoint_Random(spawnpoints);

    self spawn(spawnpoint.origin, spawnpoint.angles);
}

spawnIntermission()
{
    self notify("spawned");
    self notify("end_respawn");
    
    self setSpawnVariables();
    
    self clearLowerMessage();
    
    self freezeControls( false );
    
    self.sessionstate = "intermission";
    self.spectatorclient = -1;
    self.killcamentity = -1;
    self.archivetime = 0;
    self.psoffsettime = 0;
    self.friendlydamage = undefined;
    
    [[level.onSpawnIntermission]]();
    self setDepthOfField( 0, 128, 512, 4000, 6, 1.8 );
}


default_onSpawnIntermission()
{
    spawnpointname = "mp_global_intermission";
    spawnpoints = getentarray(spawnpointname, "classname");
//    spawnpoint = maps\mp\gametypes\_spawnlogic::getSpawnpoint_Random(spawnpoints);
    spawnpoint = spawnPoints[0];
    
    if( isDefined( spawnpoint ) )
        self spawn( spawnpoint.origin, spawnpoint.angles );
    else
        self spawn( (0, 0, 0), (0, 0, 0) ); // Who cares if there aren't any spawnpoints :P
}

// returns the best guess of the exact time until the scoreboard will be displayed and player control will be lost.
// returns undefined if time is not known
timeUntilRoundEnd()
{
    if ( level.gameEnded )
    {
        timePassed = (getTime() - level.gameEndTime) / 1000;
        timeRemaining = level.postRoundTime - timePassed;
        
        if ( timeRemaining < 0 )
            return 0;
        
        return timeRemaining;
    }
    
    if ( level.inOvertime )
        return undefined;
    
    if ( level.timeLimit <= 0 )
        return undefined;
    
    if ( !isDefined( level.startTime ) )
        return undefined;
    
    timePassed = (getTime() - level.startTime)/1000;
    timeRemaining = (level.timeLimit * 60) - timePassed;
    
    return timeRemaining + level.postRoundTime;
}

freezePlayerForRoundEnd()
{
    self clearLowerMessage();
    
    self closeMenu();
    self closeInGameMenu();
    
    self freezeControls( true );
//    self disableWeapons();
}

getHostPlayer()
{
    players = getEntArray( "player", "classname" );
    
    for ( index = 0; index < players.size; index++ )
    {
        if ( players[index] getEntityNumber() == 0 )
            return players[index];
    }
}

hostIdledOut()
{
    hostPlayer = getHostPlayer();
    
    // host never spawned
    if ( isDefined( hostPlayer ) && !hostPlayer.hasSpawned && !isDefined( hostPlayer.selectedClass ) )
        return true;

    return false;
}

endGame( endReasonText )
{
    // return if already ending via host quit or victory
    if ( game["state"] == "postgame" )
        return;

    if ( isDefined( level.onEndGame ) )
        [[level.onEndGame]]( );

    visionSetNaked( "mpOutro", 2.0 );
    
    game["state"] = "postgame";
    level.gameEndTime = getTime();
    level.gameEnded = true;
    level.inGracePeriod = false;
    level notify ( "game_ended" );
    
    setGameEndTime( 0 ); // stop/hide the timers

    // freeze players
    players = level.players;
    for ( index = 0; index < players.size; index++ )
    {
        player = players[index];
        
        player freezePlayerForRoundEnd();
        player thread roundEndDoF( 4.0 );
    }

    endReasonText = game["strings"]["time_limit_reached"];
    
    roundEndWait( level.postRoundTime, true );
    
    level.intermission = true;
    
    //regain players array since some might've disconnected during the wait above
    players = level.players;
    for ( index = 0; index < players.size; index++ )
    {
        player = players[index];
        
        player closeMenu();
        player closeInGameMenu();
        player notify ( "reset_outcome" );
        player thread spawnIntermission();
        player setClientDvar( "ui_hud_hardcore", 0 );
    }
    
    logString( "game ended" );
    wait 5.0; //scoreboard time 5 sec
    
    exitLevel( false );
}

roundEndWait( defaultDelay, matchBonus )
{
    notifiesDone = false;
    while ( !notifiesDone )
    {
        players = level.players;
        notifiesDone = true;
        for ( index = 0; index < players.size; index++ )
        {
            if ( !isDefined( players[index].doingNotify ) || !players[index].doingNotify )
                continue;
                
            notifiesDone = false;
        }
        wait ( 0.5 );
    }

    if ( !matchBonus )
    {
        wait ( defaultDelay );
        return;
    }

    wait ( defaultDelay / 2 );
    level notify ( "give_match_bonus" );
    wait ( defaultDelay / 2 );

    notifiesDone = false;
    while ( !notifiesDone )
    {
        players = level.players;
        notifiesDone = true;
        for ( index = 0; index < players.size; index++ )
        {
            if ( !isDefined( players[index].doingNotify ) || !players[index].doingNotify )
                continue;
                
            notifiesDone = false;
        }
        wait ( 0.5 );
    }
}

roundEndDOF( time )
{
    self setDepthOfField( 0, 128, 512, 4000, 6, 1.8 );
}

checkTimeLimit()
{
    if ( isDefined( level.timeLimitOverride ) && level.timeLimitOverride )
        return;
    
    if ( game["state"] != "playing" )
    {
        setGameEndTime( 0 );
        return;
    }
        
    if ( level.timeLimit <= 0 )
    {
        setGameEndTime( 0 );
        return;
    }
        
    if ( level.inPrematchPeriod )
    {
        setGameEndTime( 0 );
        return;
    }
    
    if ( !isdefined( level.startTime ) )
        return;
    
    timeLeft = getTimeRemaining();
    
    // want this accurate to the millisecond
    setGameEndTime( getTime() + int(timeLeft) );
    
    if ( timeLeft > 0 )
        return;
    
    [[level.onTimeLimit]]();
}

getTimeRemaining()
{
    return level.timeLimit * 60 * 1000 - getTimePassed();
}

registerTimeLimitDvar( dvarString, defaultValue, minValue, maxValue )
{
    dvarString = ("scr_" + dvarString + "_timelimit");
    if ( getDvar( dvarString ) == "" )
        setDvar( dvarString, defaultValue );
        
    if ( getDvarFloat( dvarString ) > maxValue )
        setDvar( dvarString, maxValue );
    else if ( getDvarFloat( dvarString ) < minValue )
        setDvar( dvarString, minValue );
        
    level.timeLimitDvar = dvarString;    
    level.timelimitMin = minValue;
    level.timelimitMax = maxValue;
    level.timelimit = getDvarFloat( level.timeLimitDvar );
    
    setDvar( "ui_timelimit", level.timelimit );
}

getValueInRange( value, minValue, maxValue )
{
    if ( value > maxValue )
        return maxValue;
    else if ( value < minValue )
        return minValue;
    else
        return value;
}

updateGameTypeDvars()
{
    level endon ( "game_ended" );
    
    while ( game["state"] == "playing" )
    {
        timeLimit = getValueInRange( getDvarFloat( level.timeLimitDvar ), level.timeLimitMin, level.timeLimitMax );
        if ( timeLimit != level.timeLimit )
        {
            level.timeLimit = timeLimit;
            setDvar( "ui_timelimit", level.timeLimit );
            level notify ( "update_timelimit" );
        }
        thread checkTimeLimit();
        
        // make sure we check time limit right when game ends
        if ( isdefined( level.startTime ) )
        {
            if ( getTimeRemaining() < 3000 )
            {
                wait .1;
                continue;
            }
        }
        wait 1;
    }
}

closeMenus()
{
    self closeMenu();
    self closeInGameMenu();
}

menuStartPlaying()
{
    self closeMenus();
    
    if(self.pers["team"] != "allies")
    {
        if(self.sessionstate == "playing")
        {
            // Why bother clicking this then!
            return;
        }

        self.pers["class"] = undefined;
        self.class = undefined;
        self.pers["team"] = "allies";
        self.team = "allies";
        self.pers["weapon"] = undefined;
        self.pers["savedmodel"] = undefined;
        self.sessionteam = "none";

        self setclientdvar("g_scriptMainMenu", game["menu_class_allies"]);
        
        self thread forceSpawn();

        self notify("joined_team");
        self notify("end_respawn");
    }
}

menuSpectator()
{
    self closeMenus();
    
    if(self.pers["team"] != "spectator")
    {
        if(isAlive(self))
        {
            self.switching_teams = true;
            self.joining_team = "spectator";
            self.leaving_team = self.pers["team"];
            self suicide();
        }

        self.pers["class"] = undefined;
        self.class = undefined;
        self.pers["team"] = "spectator";
        self.team = "spectator";
        self.pers["weapon"] = undefined;
        self.pers["savedmodel"] = undefined;

        self.sessionteam = "spectator";
        [[level.spawnSpectator]]();

        self setclientdvar("g_scriptMainMenu", game["menu_team"]);

        self notify("joined_spectators");
    }
}

timeLimitClock()
{
    level endon ( "game_ended" );
    
    wait .05;
    
    clockObject = spawn( "script_origin", (0,0,0) );
    
    while ( game["state"] == "playing" )
    {
        if ( !level.timerStopped && level.timeLimit )
        {
            timeLeft = getTimeRemaining() / 1000;
            timeLeftInt = int(timeLeft + 0.5); // adding .5 and flooring rounds it.
            
            if ( timeLeftInt >= 30 && timeLeftInt <= 60 )
                level notify ( "match_ending_soon" );
                
            if ( timeLeftInt <= 10 || (timeLeftInt <= 30 && timeLeftInt % 2 == 0) )
            {
                level notify ( "match_ending_very_soon" );
                // don't play a tick at exactly 0 seconds, that's when something should be happening!
                if ( timeLeftInt == 0 )
                    break;
                
                clockObject playSound( "ui_mp_timer_countdown" );
            }
            
            // synchronize to be exactly on the second
            if ( timeLeft - floor(timeLeft) >= .05 )
                wait timeLeft - floor(timeLeft);
        }

        wait ( 1.0 );
    }
}

gameTimer()
{
    level endon ( "game_ended" );
    
    level waittill("prematch_over");
    
    level.startTime = getTime();
    level.discardTime = 0;
    
    if ( isDefined( game["roundMillisecondsAlreadyPassed"] ) )
    {
        level.startTime -= game["roundMillisecondsAlreadyPassed"];
        game["roundMillisecondsAlreadyPassed"] = undefined;
    }
    
    prevtime = gettime();
    
    while ( game["state"] == "playing" )
    {
        if ( !level.timerStopped )
        {
            // the wait isn't always exactly 1 second. dunno why.
            game["timepassed"] += gettime() - prevtime;
        }
        prevtime = gettime();
        wait ( 1.0 );
    }
}

getTimePassed()
{
    if ( !isDefined( level.startTime ) )
        return 0;
    
    if ( level.timerStopped )
        return (level.timerPauseTime - level.startTime) - level.discardTime;
    else
        return (gettime()            - level.startTime) - level.discardTime;

}

pauseTimer()
{
    if ( level.timerStopped )
        return;
    
    level.timerStopped = true;
    level.timerPauseTime = gettime();
}

resumeTimer()
{
    if ( !level.timerStopped )
        return;
    
    level.timerStopped = false;
    level.discardTime += gettime() - level.timerPauseTime;
}

startGame()
{
    thread gameTimer();
    level.timerStopped = false;
    thread maps\mp\gametypes\_spawnlogic::spawnPerFrameUpdate();

    thread timeLimitClock();
}

spawnClient( )
{
    assert(    isDefined( self.team ) );
    assert(    isValidClass( self.class ) );
    
    self endon ( "disconnect" );
    self endon ( "end_respawn" );
    self endon ( "game_ended" );
    
    self clearLowerMessage();
    
    // No messing around, just respawn the player instantly
    self thread    [[level.spawnPlayer]]();
}

waitForTimeOrNotify( time, notifyname )
{
    self endon( notifyname );
    wait time;
}

removeSpawnMessageShortly( delay )
{
    self endon("disconnect");
    
    waittillframeend; // so we don't endon the end_respawn from spawning as a spectator
    
    self endon("end_respawn");
    
    wait delay;
    
    self clearLowerMessage( 2.0 );
}

// We only support CJ gametype, so most of this goes in the trash
Callback_StartGameType()
{
    level.intermission = false;

    if ( !isDefined( game["gamestarted"] ) )
    {
        // defaults if not defined in level script
        if ( !isDefined( game["allies"] ) )
            game["allies"] = "marines";
        if ( !isDefined( game["axis"] ) )
            game["axis"] = "opfor";
        if ( !isDefined( game["attackers"] ) )
            game["attackers"] = "allies";
        if (  !isDefined( game["defenders"] ) )
            game["defenders"] = "axis";

        if ( !isDefined( game["state"] ) )
            game["state"] = "playing";
    
        precacheStatusIcon( "hud_status_dead" );
        precacheStatusIcon( "hud_status_connecting" );
        
        precacheRumble( "damage_heavy" );

        precacheShader( "white" );
        precacheShader( "black" );
        
        makeDvarServerInfo( "scr_allies", "usmc" );
        makeDvarServerInfo( "scr_axis", "arab" );
        
        game["strings"]["press_to_spawn"] = &"PLATFORM_PRESS_TO_SPAWN";
        if ( level.teamBased )
        {
            game["strings"]["waiting_for_teams"] = &"MP_WAITING_FOR_TEAMS";
            game["strings"]["opponent_forfeiting_in"] = &"MP_OPPONENT_FORFEITING_IN";
        }
        else
        {
            game["strings"]["waiting_for_teams"] = &"MP_WAITING_FOR_PLAYERS";
            game["strings"]["opponent_forfeiting_in"] = &"MP_OPPONENT_FORFEITING_IN";
        }
        game["strings"]["match_starting_in"] = &"MP_MATCH_STARTING_IN";
        game["strings"]["spawn_next_round"] = &"MP_SPAWN_NEXT_ROUND";
        game["strings"]["waiting_to_spawn"] = &"MP_WAITING_TO_SPAWN";
        game["strings"]["match_starting"] = &"MP_MATCH_STARTING";
        game["strings"]["change_class"] = &"MP_CHANGE_CLASS_NEXT_SPAWN";
        game["strings"]["last_stand"] = &"MPUI_LAST_STAND";
        
        game["strings"]["cowards_way"] = &"PLATFORM_COWARDS_WAY_OUT";
        
        game["strings"]["tie"] = &"MP_MATCH_TIE";
        game["strings"]["round_draw"] = &"MP_ROUND_DRAW";

        game["strings"]["enemies_eliminated"] = &"MP_ENEMIES_ELIMINATED";
        game["strings"]["score_limit_reached"] = &"MP_SCORE_LIMIT_REACHED";
        game["strings"]["round_limit_reached"] = &"MP_ROUND_LIMIT_REACHED";
        game["strings"]["time_limit_reached"] = &"MP_TIME_LIMIT_REACHED";
        game["strings"]["players_forfeited"] = &"MP_PLAYERS_FORFEITED";

        switch ( game["allies"] )
        {
            case "sas":
                game["strings"]["allies_win"] = &"MP_SAS_WIN_MATCH";
                game["strings"]["allies_win_round"] = &"MP_SAS_WIN_ROUND";
                game["strings"]["allies_mission_accomplished"] = &"MP_SAS_MISSION_ACCOMPLISHED";
                game["strings"]["allies_eliminated"] = &"MP_SAS_ELIMINATED";
                game["strings"]["allies_forfeited"] = &"MP_SAS_FORFEITED";
                game["strings"]["allies_name"] = &"MP_SAS_NAME";
                
                game["music"]["spawn_allies"] = "mp_spawn_sas";
                game["music"]["victory_allies"] = "mp_victory_sas";
                game["icons"]["allies"] = "faction_128_sas";
                game["colors"]["allies"] = (0.6,0.64,0.69);
                game["voice"]["allies"] = "UK_1mc_";
                setDvar( "scr_allies", "sas" );
                break;
            case "marines":
            default:
                game["strings"]["allies_win"] = &"MP_MARINES_WIN_MATCH";
                game["strings"]["allies_win_round"] = &"MP_MARINES_WIN_ROUND";
                game["strings"]["allies_mission_accomplished"] = &"MP_MARINES_MISSION_ACCOMPLISHED";
                game["strings"]["allies_eliminated"] = &"MP_MARINES_ELIMINATED";
                game["strings"]["allies_forfeited"] = &"MP_MARINES_FORFEITED";
                game["strings"]["allies_name"] = &"MP_MARINES_NAME";
                
                game["music"]["spawn_allies"] = "mp_spawn_usa";
                game["music"]["victory_allies"] = "mp_victory_usa";
                game["icons"]["allies"] = "faction_128_usmc";
                game["colors"]["allies"] = (0,0,0);
                game["voice"]["allies"] = "US_1mc_";
                setDvar( "scr_allies", "usmc" );
                break;
        }
        switch ( game["axis"] )
        {
            case "russian":
                game["strings"]["axis_win"] = &"MP_SPETSNAZ_WIN_MATCH";
                game["strings"]["axis_win_round"] = &"MP_SPETSNAZ_WIN_ROUND";
                game["strings"]["axis_mission_accomplished"] = &"MP_SPETSNAZ_MISSION_ACCOMPLISHED";
                game["strings"]["axis_eliminated"] = &"MP_SPETSNAZ_ELIMINATED";
                game["strings"]["axis_forfeited"] = &"MP_SPETSNAZ_FORFEITED";
                game["strings"]["axis_name"] = &"MP_SPETSNAZ_NAME";
                
                game["music"]["spawn_axis"] = "mp_spawn_soviet";
                game["music"]["victory_axis"] = "mp_victory_soviet";
                game["icons"]["axis"] = "faction_128_ussr";
                game["colors"]["axis"] = (0.52,0.28,0.28);
                game["voice"]["axis"] = "RU_1mc_";
                setDvar( "scr_axis", "ussr" );
                break;
            case "arab":
            case "opfor":
            default:
                game["strings"]["axis_win"] = &"MP_OPFOR_WIN_MATCH";
                game["strings"]["axis_win_round"] = &"MP_OPFOR_WIN_ROUND";
                game["strings"]["axis_mission_accomplished"] = &"MP_OPFOR_MISSION_ACCOMPLISHED";
                game["strings"]["axis_eliminated"] = &"MP_OPFOR_ELIMINATED";
                game["strings"]["axis_forfeited"] = &"MP_OPFOR_FORFEITED";
                game["strings"]["axis_name"] = &"MP_OPFOR_NAME";
                
                game["music"]["spawn_axis"] = "mp_spawn_opfor";
                game["music"]["victory_axis"] = "mp_victory_opfor";
                game["icons"]["axis"] = "faction_128_arab";
                game["colors"]["axis"] = (0.65,0.57,0.41);
                game["voice"]["axis"] = "AB_1mc_";
                setDvar( "scr_axis", "arab" );
                break;
        }
        game["music"]["defeat"] = "mp_defeat";
        game["music"]["victory_spectator"] = "mp_defeat";
        game["music"]["winning"] = "mp_time_running_out_winning";
        game["music"]["losing"] = "mp_time_running_out_losing";
        game["music"]["victory_tie"] = "mp_defeat";
        
        game["music"]["suspense"] = [];
        game["music"]["suspense"][game["music"]["suspense"].size] = "mp_suspense_01";
        game["music"]["suspense"][game["music"]["suspense"].size] = "mp_suspense_02";
        game["music"]["suspense"][game["music"]["suspense"].size] = "mp_suspense_03";
        game["music"]["suspense"][game["music"]["suspense"].size] = "mp_suspense_04";
        game["music"]["suspense"][game["music"]["suspense"].size] = "mp_suspense_05";
        game["music"]["suspense"][game["music"]["suspense"].size] = "mp_suspense_06";
        
        game["dialog"]["mission_success"] = "mission_success";
        game["dialog"]["mission_failure"] = "mission_fail";
        game["dialog"]["mission_draw"] = "draw";

        game["dialog"]["round_success"] = "encourage_win";
        game["dialog"]["round_failure"] = "encourage_lost";
        game["dialog"]["round_draw"] = "draw";
        
        // status
        game["dialog"]["timesup"] = "timesup";
        game["dialog"]["winning"] = "winning";
        game["dialog"]["losing"] = "losing";
        game["dialog"]["lead_lost"] = "lead_lost";
        game["dialog"]["lead_tied"] = "tied";
        game["dialog"]["lead_taken"] = "lead_taken";
        game["dialog"]["last_alive"] = "lastalive";

        game["dialog"]["boost"] = "boost";

        if ( !isDefined( game["dialog"]["offense_obj"] ) )
            game["dialog"]["offense_obj"] = "boost";
        if ( !isDefined( game["dialog"]["defense_obj"] ) )
            game["dialog"]["defense_obj"] = "boost";
        
        game["dialog"]["hardcore"] = "hardcore";
        game["dialog"]["oldschool"] = "oldschool";
        game["dialog"]["highspeed"] = "highspeed";
        game["dialog"]["tactical"] = "tactical";

        game["dialog"]["challenge"] = "challengecomplete";
        game["dialog"]["promotion"] = "promotion";

        game["dialog"]["bomb_taken"] = "bomb_taken";
        game["dialog"]["bomb_lost"] = "bomb_lost";
        game["dialog"]["bomb_defused"] = "bomb_defused";
        game["dialog"]["bomb_planted"] = "bomb_planted";

        game["dialog"]["obj_taken"] = "securedobj";
        game["dialog"]["obj_lost"] = "lostobj";

        game["dialog"]["obj_defend"] = "obj_defend";
        game["dialog"]["obj_destroy"] = "obj_destroy";
        game["dialog"]["obj_capture"] = "capture_obj";
        game["dialog"]["objs_capture"] = "capture_objs";

        game["dialog"]["hq_located"] = "hq_located";
        game["dialog"]["hq_enemy_captured"] = "hq_captured";
        game["dialog"]["hq_enemy_destroyed"] = "hq_destroyed";
        game["dialog"]["hq_secured"] = "hq_secured";
        game["dialog"]["hq_offline"] = "hq_offline";
        game["dialog"]["hq_online"] = "hq_online";

        game["dialog"]["move_to_new"] = "new_positions";

        game["dialog"]["attack"] = "attack";
        game["dialog"]["defend"] = "defend";
        game["dialog"]["offense"] = "offense";
        game["dialog"]["defense"] = "defense";

        game["dialog"]["halftime"] = "halftime";
        game["dialog"]["overtime"] = "overtime";
        game["dialog"]["side_switch"] = "switching";

        game["dialog"]["flag_taken"] = "ourflag";
        game["dialog"]["flag_dropped"] = "ourflag_drop";
        game["dialog"]["flag_returned"] = "ourflag_return";
        game["dialog"]["flag_captured"] = "ourflag_capt";
        game["dialog"]["enemy_flag_taken"] = "enemyflag";
        game["dialog"]["enemy_flag_dropped"] = "enemyflag_drop";
        game["dialog"]["enemy_flag_returned"] = "enemyflag_return";
        game["dialog"]["enemy_flag_captured"] = "enemyflag_capt";

        game["dialog"]["capturing_a"] = "capturing_a";
        game["dialog"]["capturing_b"] = "capturing_b";
        game["dialog"]["capturing_c"] = "capturing_c";
        game["dialog"]["captured_a"] = "capture_a";
        game["dialog"]["captured_b"] = "capture_c";
        game["dialog"]["captured_c"] = "capture_b";

        game["dialog"]["securing_a"] = "securing_a";
        game["dialog"]["securing_b"] = "securing_b";
        game["dialog"]["securing_c"] = "securing_c";
        game["dialog"]["secured_a"] = "secure_a";
        game["dialog"]["secured_b"] = "secure_b";
        game["dialog"]["secured_c"] = "secure_c";

        game["dialog"]["losing_a"] = "losing_a";
        game["dialog"]["losing_b"] = "losing_b";
        game["dialog"]["losing_c"] = "losing_c";
        game["dialog"]["lost_a"] = "lost_a";
        game["dialog"]["lost_b"] = "lost_b";
        game["dialog"]["lost_c"] = "lost_c";

        game["dialog"]["enemy_taking_a"] = "enemy_take_a";
        game["dialog"]["enemy_taking_b"] = "enemy_take_b";
        game["dialog"]["enemy_taking_c"] = "enemy_take_c";
        game["dialog"]["enemy_has_a"] = "enemy_has_a";
        game["dialog"]["enemy_has_b"] = "enemy_has_b";
        game["dialog"]["enemy_has_c"] = "enemy_has_c";

        game["dialog"]["lost_all"] = "take_positions";
        game["dialog"]["secure_all"] = "positions_lock";

        [[level.onPrecacheGameType]]();

        game["gamestarted"] = true;
        
        game["teamScores"]["allies"] = 0;
        game["teamScores"]["axis"] = 0;
    }

    if(!isdefined(game["timepassed"]))
        game["timepassed"] = 0;

    if(!isdefined(game["roundsplayed"]))
        game["roundsplayed"] = 0;
    
    level.skipVote = false;
    level.gameEnded = false;
    level.teamSpawnPoints["axis"] = [];
    level.teamSpawnPoints["allies"] = [];

    level.objIDStart = 0;
    level.forcedEnd = false;
    level.hostForcedEnd = false;

    level.hardcoreMode = getDvarInt( "scr_hardcore" );
    if ( level.hardcoreMode )
        logString( "game mode: hardcore" );

    // this gets set to false when someone takes damage or a gametype-specific event happens.
    level.useStartSpawns = true;
    
    // set to 0 to disable
    if ( getdvar( "scr_teamKillPunishCount" ) == "" )
        setdvar( "scr_teamKillPunishCount", "3" );
    level.minimumAllowedTeamKills = getdvarint( "scr_teamKillPunishCount" ) - 1; // punishment starts at the next one
    
    if( getdvar( "r_reflectionProbeGenerate" ) == "1" )
        level waittill( "eternity" );

    thread maps\mp\gametypes\_class::init();
    thread maps\mp\gametypes\_rank::init();
    thread maps\mp\gametypes\_menus::init();
    thread maps\mp\gametypes\_hud::init();
    thread maps\mp\gametypes\_serversettings::init();
    thread maps\mp\gametypes\_clientids::init();
    thread maps\mp\gametypes\_teams::init();
    thread maps\mp\gametypes\_weapons::init();
    thread maps\mp\gametypes\_scoreboard::init();
    thread maps\mp\gametypes\_killcam::init();
    thread maps\mp\gametypes\_shellshock::init();
    thread maps\mp\gametypes\_damagefeedback::init();
    thread maps\mp\gametypes\_healthoverlay::init();
    thread maps\mp\gametypes\_spectating::init();
    thread maps\mp\gametypes\_gameobjects::init();
    thread maps\mp\gametypes\_spawnlogic::init();
    thread maps\mp\gametypes\_battlechatter_mp::init();
    thread maps\mp\gametypes\_oldschool::deletePickups();
        
    thread maps\mp\gametypes\_hud_message::init();

    thread maps\mp\gametypes\_quickmessages::init();

    stringNames = getArrayKeys( game["strings"] );
    for ( index = 0; index < stringNames.size; index++ )
        precacheString( game["strings"][stringNames[index]] );

    level.maxPlayerCount = 0;
    level.playerCount["allies"] = 0;
    level.playerCount["axis"] = 0;
    level.aliveCount["allies"] = 0;
    level.aliveCount["axis"] = 0;
    level.playerLives["allies"] = 0;
    level.playerLives["axis"] = 0;
    level.lastAliveCount["allies"] = 0;
    level.lastAliveCount["axis"] = 0;
    level.everExisted["allies"] = false;
    level.everExisted["axis"] = false;
    level.waveDelay["allies"] = 0;
    level.waveDelay["axis"] = 0;
    level.lastWave["allies"] = 0;
    level.lastWave["axis"] = 0;
    level.wavePlayerSpawnIndex["allies"] = 0;
    level.wavePlayerSpawnIndex["axis"] = 0;
    level.alivePlayers["allies"] = [];
    level.alivePlayers["axis"] = [];
    level.activePlayers = [];

    if ( !isDefined( level.timeLimit ) )
        registerTimeLimitDvar( "default", 10, 1, 1440 );

    makeDvarServerInfo( "ui_scorelimit", 0 );
    makeDvarServerInfo( "ui_timelimit", 60000 );
    makeDvarServerInfo( "ui_allow_classchange", 0 );
    makeDvarServerInfo( "ui_allow_teamchange", 0 );


    if ( getDvarInt( "scr_game_forceuav" ) )
        setDvar( "g_compassShowEnemies", 1 );
    else
        setDvar( "g_compassShowEnemies", 0 );
    
    // No pre-match crap in CJ
    level.inPrematchPeriod = false;
    level.inGracePeriod = false;
    
    [[level.onStartGameType]]();
    
    thread startGame();
    level thread updateGameTypeDvars();
}

listenForGameEnd()
{
    self waittill( "host_sucks_end_game" ); // Lol
    if ( level.console )
        endparty();
    level.skipVote = true;

    if ( !level.gameEnded )
        level thread maps\mp\gametypes\_globallogic::forceEnd();
}

Callback_PlayerConnect()
{
    thread notifyConnecting();

    self.statusicon = "hud_status_connecting";
    self waittill( "begin" );
    waittillframeend;
    self.statusicon = "";

    level notify( "connected", self );

    iPrintLn(&"MP_CONNECTED", self);

    lpselfnum = self getEntityNumber();
    lpGuid = self getGuid();
    logPrint("J;" + lpGuid + ";" + lpselfnum + ";" + self.name + "\n");
    
    self.leaderDialogQueue = [];
    self.leaderDialogActive = false;
    self.leaderDialogGroups = [];
    self.leaderDialogGroup = "";
    
    self.pers["lives"] = level.numLives;
    
    self.hasSpawned = false;
    self.waitingToSpawn = false;
    self.deathCount = 0;
    
    level.players[level.players.size] = self;
    
    [[level.onPlayerConnect]]();
    
    // When joining a game in progress, if the game is at the post game state (scoreboard) the connecting player should spawn into intermission
    if ( game["state"] == "postgame" )
    {
        self.pers["team"] = "spectator";
        self.team = "spectator";

        self setClientDvars( "ui_hud_hardcore", 1, "cg_drawSpectatorMessages", 0 );
        
        [[level.spawnIntermission]]();
        self closeMenu();
        self closeInGameMenu();
        return;
    }

    level endon( "game_ended" );

    if ( isDefined( self.pers["team"] ) )
        self.team = self.pers["team"];

    if ( isDefined( self.pers["class"] ) )
        self.class = self.pers["class"];
    
    // Spawn the player directly after connecting
    self.pers["team"] = "spectator";
    self.team = "spectator";
    self.sessionstate = "dead";
    self thread [[level.autoassign]]();
}

// Handy. Don't have to do any coding to spawn players instantly after connecting
forceSpawn()
{
    self endon ( "death" );
    self endon ( "disconnect" );
    self endon ( "spawned" );
    
    if ( self.pers["team"] == "spectator" )
        return;
    
    self.pers["class"] = "CLASS_CUSTOM1";
    self.class = self.pers["class"];
    
    self closeMenus();
    
    self thread [[level.spawnClient]]();
}

Callback_PlayerDisconnect()
{
    self removePlayerOnDisconnect();
    
    [[level.onPlayerDisconnect]]();
    
    // Fill in the gap potentially caused by the disconnect
    for ( entry = 0; entry < level.players.size; entry++ )
    {
        if ( level.players[entry] == self )
        {
            while ( entry < level.players.size-1 )
            {
                level.players[entry] = level.players[entry+1];
                entry++;
            }
            level.players[entry] = undefined;
            break;
        }
    }
}

// Keep track of players that are actually in the server
removePlayerOnDisconnect()
{
    for ( entry = 0; entry < level.players.size; entry++ )
    {
        if ( level.players[entry] == self )
        {
            while ( entry < level.players.size-1 )
            {
                level.players[entry] = level.players[entry+1];
                entry++;
            }
            level.players[entry] = undefined;
            break;
        }
    }
}

// Maybe we need this callback sometime. For now it's empty.
Callback_PlayerDamage( eInflictor, eAttacker, iDamage, iDFlags, sMeansOfDeath, sWeapon, vPoint, vDir, sHitLoc, psOffsetTime )
{
    if ( game["state"] == "postgame" )
        return;
    
    if ( self.sessionteam == "spectator" )
        return;
    
    if ( isDefined( self.canDoCombat ) && !self.canDoCombat )
        return;
    
    if ( isDefined( eAttacker ) && isPlayer( eAttacker ) && isDefined( eAttacker.canDoCombat ) && !eAttacker.canDoCombat )
        return;
}

finishPlayerDamageWrapper( eInflictor, eAttacker, iDamage, iDFlags, sMeansOfDeath, sWeapon, vPoint, vDir, sHitLoc, psOffsetTime )
{
    self finishPlayerDamage( eInflictor, eAttacker, iDamage, iDFlags, sMeansOfDeath, sWeapon, vPoint, vDir, sHitLoc, psOffsetTime );
    
    self thread maps\mp\gametypes\_weapons::onWeaponDamage( eInflictor, sWeapon, sMeansOfDeath, iDamage );
}

Callback_PlayerKilled(eInflictor, attacker, iDamage, sMeansOfDeath, sWeapon, vDir, sHitLoc, psOffsetTime, deathAnimDuration)
{
    self endon( "spawned" );
    self notify( "killed_player" );
    
    if ( self.sessionteam == "spectator" )
        return;
    
    if ( game["state"] == "postgame" )
        return;

    maps\mp\gametypes\_spawnlogic::deathOccured(self, attacker);

    // Maybe some other scripts rely on these variables, so set them
    self.sessionstate = "dead";
    self.statusicon = "hud_status_dead";
    self.pers["weapon"] = undefined;
    self.killedPlayersCurrent = [];
    self.deathCount++;

    self.switching_teams = undefined;
    self.joining_team = undefined;
    self.leaving_team = undefined;

    // Don't do killcam stuff, but pass this through to the following function
    self thread [[level.onPlayerKilled]](eInflictor, attacker, iDamage, sMeansOfDeath, sWeapon, vDir, sHitLoc, psOffsetTime, deathAnimDuration);
    
    // Maybe some other scripts rely on these variables, so set them
    self.deathTime = getTime();
    self notify ( "death_delay_finished" );
    
    if ( game["state"] != "playing" )
    {
        self.sessionstate = "dead";
        self.spectatorclient = -1;
        self.killcamentity = -1;
        self.archivetime = 0;
        self.psoffsettime = 0;
        return;
    }
    
    self thread [[level.spawnClient]]();
}

cancelKillCamOnUse()
{
    self endon ( "death_delay_finished" );
    self endon ( "disconnect" );
    level endon ( "game_ended" );
    
    for ( ;; )
    {
        if ( !self UseButtonPressed() )
        {
            wait ( 0.05 );
            continue;
        }
        
        buttonTime = 0;
        while( self UseButtonPressed() )
        {
            buttonTime += 0.05;
            wait ( 0.05 );
        }
        
        if ( buttonTime >= 0.5 )
            continue;
        
        buttonTime = 0;
        
        while ( !self UseButtonPressed() && buttonTime < 0.5 )
        {
            buttonTime += 0.05;
            wait ( 0.05 );
        }
        
        if ( buttonTime >= 0.5 )
            continue;
            
        self.cancelKillcam = true;
        return;
    }    
}

setSpawnVariables()
{
    resetTimeout();

    // Stop shellshock and rumble
    self StopShellshock();
    self StopRumble( "damage_heavy" );
}

notifyConnecting()
{
    waittillframeend;

    if( isDefined( self ) )
        level notify( "connecting", self );
}

// TODO: Can probably be removed, but we might need make use of this sometime
getHitLocHeight( sHitLoc )
{
    switch( sHitLoc )
    {
        case "helmet":
        case "head":
        case "neck":
            return 60;
        case "torso_upper":
        case "right_arm_upper":
        case "left_arm_upper":
        case "right_arm_lower":
        case "left_arm_lower":
        case "right_hand":
        case "left_hand":
        case "gun":
            return 48;
        case "torso_lower":
            return 40;
        case "right_leg_upper":
        case "left_leg_upper":
            return 32;
        case "right_leg_lower":
        case "left_leg_lower":
            return 10;
        case "right_foot":
        case "left_foot":
            return 5;
    }
    return 48;
}

debugLine( start, end )
{
    for ( i = 0; i < 50; i++ )
    {
        line( start, end );
        wait .05;
    }
}

