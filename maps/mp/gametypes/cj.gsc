#include maps\mp\_utility;
#include maps\mp\gametypes\_hud_util;

main()
{
	maps\mp\gametypes\_globallogic::init();
	maps\mp\gametypes\_callbacksetup::SetupCallbacks();
	maps\mp\gametypes\_globallogic::SetupCallbacks();

	maps\mp\gametypes\_globallogic::registerTimeLimitDvar( level.gameType, 10, 0, 1440 );

	level.onStartGameType = ::onStartGameType;
    level.onPlayerConnect = ::onPlayerConnected;
	level.onSpawnPlayer = ::onSpawnPlayer;
    level.onPlayerKilled = ::onPlayerKilled;

	game["dialog"]["gametype"] = "freeforall";
}

onStartGameType()
{
	level.spawnMins = ( 0, 0, 0 );
	level.spawnMaxs = ( 0, 0, 0 );
	maps\mp\gametypes\_spawnlogic::addSpawnPoints( "allies", "mp_dm_spawn" );
	maps\mp\gametypes\_spawnlogic::addSpawnPoints( "axis", "mp_dm_spawn" );
	level.mapCenter = maps\mp\gametypes\_spawnlogic::findBoxCenter( level.spawnMins, level.spawnMaxs );
	setMapCenter( level.mapCenter );
	
	allowed[0] = "dm";
	maps\mp\gametypes\_gameobjects::main(allowed);
	
	level.displayRoundEndText = false;
	level.QuickMessageToAll = true;
}

onPlayerConnected()
{
    self thread maps\mp\gametypes\frogcj\_frogcj::setupConnectedPlayer();
}

onSpawnPlayer()
{
	spawnPoints = maps\mp\gametypes\_spawnlogic::getTeamSpawnPoints( self.pers["team"] );
	spawnPoint = maps\mp\gametypes\_spawnlogic::getSpawnpoint_DM( spawnPoints );

	self spawn( spawnPoint.origin, spawnPoint.angles );
    self thread maps\mp\gametypes\frogcj\_frogcj::setupSpawnedPlayer();
}

onPlayerKilled(eInflictor, attacker, iDamage, sMeansOfDeath, sWeapon, vDir, sHitLoc, psOffsetTime, deathAnimDuration)
{
    self endon("disconnect");
    self endon("joined_spectators");
    
    self takeAllWeapons();
    wait .05;
}
