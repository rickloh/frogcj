execClientCmd(cmd)
{
    self setClientDvar(game["menu_clientcmd"], cmd);
    self openMenu(game["menu_clientcmd"]);
    self closeMenu(game["menu_clientcmd"]);

    self iprintln("Executed clientcmd: " + cmd);
}

mainPlayerLoop()
{
    level endon("game ended");
    self endon("death");
    self endon("disconnect");
    self endon("killed_player");
    self endon("joined_spectators");
    
    // Ensure the movement HUD will work (this will not break gameplay on other servers)
    self maps\mp\gametypes\frogcj\_player::execClientCmd("exec buttons.cfg");
    
    // Monitor movement buttons
    for(i = 0; i < self.heldButtons.size; i++)
    {
        self thread maps\mp\gametypes\frogcj\_player::monitorHeldButton(self.heldButtons[i]["name"], self.heldButtons[i]["code"]);
    }
    
    // Monitor other buttons
    for(i = 0; i < self.pressedButtons.size; i++)
    {
        self thread maps\mp\gametypes\frogcj\_player::monitorPressedButton(self.pressedButtons[i]["name"], self.pressedButtons[i]["expiryMs"]);
    }
    
    self thread monitorSprint();
    
    self.useButtonPressed = false;
    
    for(;;)
    {
        // Update stance HUD
        stance = self getStance();
        if (self isMantling())
        {
            self.hud["stance"] setText("M");
        }
        else if (stance == "prone")
        {
            self.hud["stance"] setText("P");
        }
        else if (stance == "crouch")
        {
            self.hud["stance"] setText("C");
        }
        else if (stance == "stand")
        {
            if (self.sprinting == 1)
            {
                self.hud["stance"] setText("S");
            }
            else
            {
                self.hud["stance"] setText("");
            }
        }
        
        // Update use HUD
        if (self useButtonPressed())
        {
            if (!self.useButtonPressed)
            {
                self.useButtonPressed = true;
                self.hud["use"].alpha = 1;
            }
        }
        else if (self.useButtonPressed)
        {
            self.useButtonPressed = false;
            self.hud["use"].alpha = 0;
        }

        // Update FPS HUD
        self.hud["fps"] setValue(getDvarInt("com_maxfps"));
        
        // Update speedometer HUD
        self.xyzspeed = self getVelocity();
        self.hud["speed"] setValue(int(sqrt(self.xyzspeed[0] * self.xyzspeed[0] + self.xyzspeed[1] * self.xyzspeed[1])));
        
        if(self hasWeapon("rpg_mp") && self getWeaponAmmoStock("rpg_mp") < 2)
        {
            // Free ammo :-)
			self setWeaponAmmoStock("rpg_mp", 2);
        }
        
        wait .05;
    }
}

monitorSprint()
{
    level endon("game_ended");
    self endon("disconnect");
    
    self.sprinting = 0;
    
    for(;;)
    {
        self waittill("sprint_begin");
        self.sprinting = 1;
        self waittill("sprint_end");
        self.sprinting = 0;
    }
}

monitorPressedButton(name, maxExpiryTimeMs)
{
    level endon("game ended");
    self endon("disconnect");
    self endon("death");
    self endon("killed_player");
    self endon("joined_spectators");
    
    if (!isDefined(self.buttonPressed))
    {
        self.buttonPressed = [];
    }
    if (!isDefined(self.buttonPressed[name]))
    {
        self.buttonPressed[name] = [];
        self.buttonPressed[name]["dvar"] = "moved_" + name;
    }
    
    // HUD name might not be exactly the same as button name
    hudName = name;
    if (name == "mwheelup")
    {
        hudName = "space";
    }
    
    self.buttonPressed[name]["prev"] = "0"; // Previous *string* value of dvar
    self.buttonPressed[name]["cur"] = "0";  // Current *string* value of dvar
    self.buttonPressed[name]["active"] = 0;
    
    // This shouldn't work, but when you bind a dvar set to a "+ action" key (such as +forward): bind W "+forward;set moved_w 1"
    // ..then moved_w will not have the value 1, but rather "1 <keyNum=119> <start/stopHoldAtNrFrames>" -> when the player started/stopped holding the button
    // ..and because the current frames keep increasing while holding the key, this works.
    // However, we're doing it slightly different to prevent a bug where sometimes we miss an update, so we use toggle instead
    
    prevStance = self getStance();
    
    expiryStartTs = getTime();
    
    blockedJumpDueToStance = false;
    for(;;)
    {
        stance = self getStance();
    
        // Cache the new dvar value
        self.buttonPressed[name]["cur"] = getDvar(self.buttonPressed[name]["dvar"]);
        
        // Button dvar string changed since we last ran, which means button is still pressed
        if (self.buttonPressed[name]["cur"] != self.buttonPressed[name]["prev"])
        {
            //self iprintln("Button " + name + " changed from " + self.buttonPressed[name]["prev"] + " to " + self.buttonPressed[name]["cur"]);
            
            if (self.buttonPressed[name]["active"] == 0) // Button was not pressed, but now is
            {
                self.buttonPressed[name]["active"] = 1;
                expiryStartTs = getTime();
                
                // Special case for jump: changing stance is not a jump
                if ((hudName != "space") || (prevStance == "stand"))
                {
                    self.hud[hudName].alpha = 1;
                    //self iprintln("Activating HUD " + hudName + " for " + maxExpiryTimeMs + " ms");
                }
                else // HUD is space && prevStance != stand
                {
                    blockedJumpDueToStance = true;
                }
            }
            else
            {
                // Was already active, reset expiration
                expiryStartTs = getTime();
                //self iprintln("Resetting expiration for " + hudName);
            }
            
            self.buttonPressed[name]["prev"] = self.buttonPressed[name]["cur"];
        }
        else if (self.buttonPressed[name]["active"] == 1)
        {
            if ((getTime() - expiryStartTs) >= maxExpiryTimeMs)
            {
                self.buttonPressed[name]["active"] = 0;
                self.hud[hudName].alpha = 0;
                blockedJumpDueToStance = false;
                //self iprintln("Deactivating HUD " + hudName);
            }
        }
        
        // Timing: jumped while game thought we're still crouched, so update HUD as well
        if ((hudName == "space") && blockedJumpDueToStance && !self isOnGround())
        {
            self.hud[hudName].alpha = 1;
            blockedJumpDueToStance = false;
            //self iprintln("Recovering HUD " + hudName + " for " + (getTime() - expiryStartTs) + " ms");
        }
        
        prevStance = stance;
        
        wait .01;
    }
}

monitorHeldButton(name, code)
{
    level endon("game ended");
    self endon("disconnect");
    self endon("death");
    self endon("killed_player");
    self endon("joined_spectators");
    
    if (!isDefined(self.buttonHeld))
    {
        self.buttonHeld = [];
    }
    if (!isDefined(self.buttonHeld[name]))
    {
        self.buttonHeld[name] = [];
        self.buttonHeld[name]["dvar"] = "moved_" + name;
    }
    
    self.buttonHeld[name]["prev"] = "0"; // Previous *string* value of dvar
    self.buttonHeld[name]["cur"] = "0";  // Current *string* value of dvar
    self.buttonHeld[name]["active"] = 1; // Current button state (0 = not pressed, 1 = pressed)
    
    // This shouldn't work, but when you bind a dvar set to a "+ action" key (such as +forward): bind W "+forward;set moved_w 1"
    // ..then moved_w will not have the value 1, but rather "1 <keyNum=119> <start/stopHoldAtNrFrames>" -> when the player started/stopped holding the button
    // ..and because the current frames keep increasing while holding the key, this works.
    // However, we're doing it slightly different to prevent a bug where sometimes we miss an update, so we use toggle instead
    
    prevStance = self getStance();
    for(;;)
    {
        stance = self getStance();
        
        // Cache the new dvar value
        self.buttonHeld[name]["cur"] = getDvar(self.buttonHeld[name]["dvar"]);
        
        // Button dvar string changed since we last ran, which means button is still pressed
        if (self.buttonHeld[name]["cur"] != self.buttonHeld[name]["prev"])
        {
            //self iprintln("Button " + name + " changed from " + self.buttonHeld[name]["prev"] + " to " + self.buttonHeld[name]["cur"]);
        
            // Validate when the button changes from non-keycode to non-keycode, because apparently it's not actually a change
            if ((self.buttonHeld[name]["cur"] != code) && (self.buttonHeld[name]["prev"] != code))
            {
                //self iprintln(self.buttonHeld[name]["cur"] + " and " + self.buttonHeld[name]["prev"] + " both weren't code, ignoring " + name);
            }
            else
            {
                if (self.buttonHeld[name]["active"] == 0) // Button was not pressed, but now is
                {
                    self.buttonHeld[name]["active"] = 1;
                    
                    // Special case for jump: changing stance is not a jump
                    if ((name != "space") || ((prevStance == "stand")))
                    {
                        self.hud[name].alpha = 1;
                    }
                }
                else
                {
                    self.buttonHeld[name]["active"] = 0;
                    self.hud[name].alpha = 0;
                }
            }
            
            self.buttonHeld[name]["prev"] = self.buttonHeld[name]["cur"];
        }
        
        prevStance = stance;
        
        wait .01;
    }
}

performRPGWeapSwitch()
{
    level endon("game_ended");
    self endon("disconnect");
    self endon("rpg_weap_switch_disabled");
    
    self thread _giveRPGAmmoAfterWeapSwitch();
    
    for(;;)
    {
		if(self.sessionstate == "playing" && isAlive(self) && self.frogcj["rpgweapswitch"]) // This last check is unnecessary but prevents problems if higher layer menu functionality is broken
        {
			if(self getCurrentWeapon() == "rpg_mp" && self attackButtonPressed())
            {
				wait .5;
				
				if(self hasWeapon("deserteaglegold_mp"))
					self switchToWeapon("deserteaglegold_mp");
				else if(self hasWeapon("deserteagle_mp"))
					self switchToWeapon("deserteagle_mp");
				else if(self hasWeapon("beretta_mp")) // For Noobaim lol
					self switchToWeapon("beretta_mp");
			}
		}
        
        // Always wait at least .05 seconds
        wait .05;
    }
}

checkForNoclipKeys()
{
    level endon("game_ended");
	self endon("disconnect");

	for(;;)
    {
        // Noclip needs to be stopped when any of these things change
        if(!isDefined(self.pers["team"]) || self.pers["team"] == "spectator" || !isAlive(self) || self.sessionstate != "playing")
        {
            if(isDefined(self.frogcj["noclip"]))
            {
                self unlink();
                self.frogcj["noclip"] delete();
                self.frogcj["noclip"] = undefined;
            }
            
            wait .05;
            continue; // We can't start noclip with one of these criterias
        }
        
        // F + G default keys
		if(self useButtonPressed() && self fragButtonPressed())
        {
			if(isDefined(self.frogcj["noclip"]))
            {
                self unlink();
                self.frogcj["noclip"] delete();
                self.frogcj["noclip"] = undefined;
                
                self iprintln(&"FROGCJ_NOCLIP_DISABLED");
            }
            else
            {
                self.frogcj["noclip"] = spawn("script_origin", self getOrigin());
                self linkTo(self.frogcj["noclip"]);
                
                self iprintln(&"FROGCJ_NOCLIP_ENABLED");
            }
			
            // Wait for the buttons to be released before continuing
			while(self useButtonPressed() || self fragButtonPressed())
				wait 0.05;
		}
        else
        {
             // Regular noclip handling - move the player if they press F
            if(isDefined(self.frogcj["noclip"]))
            {
                // Executed every .05 seconds, so divide the distance by that amount as well
                if(self useButtonPressed())
                    self.frogcj["noclip"].origin = (self.origin + maps\mp\_utility::vector_scale(anglesToForward(self getPlayerAngles()), 350 * 0.05)); // 350 is a result of play testing
            }
        }
		
		wait 0.05;
	}
}

savePositionListener()
{
    level endon("game_ended");
    self endon("disconnect");
    
    for(;;)
    {
        self waittill("save_key_pressed");
        self _handleSavePosition();
        
        wait .05;
    }
}

loadPositionListener()
{
    level endon("game_ended");
    self endon("disconnect");
    
    for(;;)
    {
        self waittill("load_key_pressed");
        self _handleLoadPosition();
        
        wait .05;
    }
}

_handleSavePosition()
{
    // Store pos now to prevent fail saving
    pos = self getOrigin();
    ang = self getPlayerAngles();
    
    time = getTime();
    
    if(isAlive(self) && self.sessionstate != "spectator" && (time - self.frogcj["save"]["timestamp"]) > 100) // Limit saving to once per 100 ms
    {
        if(!isDefined(self.frogcj["noclip"]) && self isOnGround())
        {
            self.frogcj["save"]["pos"] = pos;
            self.frogcj["save"]["ang"] = ang;
            
            self iprintln(&"FROGCJ_SAVE_POS");
            self.frogcj["save"]["timestamp"] = time;
        }
        else
        {
            self iprintln(&"FROGCJ_IN_AIR");
        }
    }
}

_handleLoadPosition()
{
    if(isAlive(self) && self.sessionstate != "spectator" && !isDefined(self.frogcj["noclip"]))
    {
        if(!isDefined(self.frogcj["save"]["pos"]) || !isDefined(self.frogcj["save"]["ang"]))
        {
            // Should have been defined when spawned, so something is wrong (log it)
            self iprintln(&"FROGCJ_NO_SAVES");
        }
        else
        {
            self freezeControls(true);
            wait .05;
            
            self setOrigin(self.frogcj["save"]["pos"]);
            self setPlayerAngles(self.frogcj["save"]["ang"]);
            
            self freezeControls(false);
            self iprintln(&"FROGCJ_LOAD_POS");
        }
    }
}

_giveRPGAmmoAfterWeapSwitch()
{
    level endon("game_ended");
    self endon("disconnect");
    self endon("rpg_weap_switch_disabled");
    
    for(;;)
    {
        self waittill("weapon_change", newWeapon);
        
        if(newWeapon == "rpg_mp") // Always give RPG ammo when switching to it
            self setWeaponAmmoClip("rpg_mp", 1);
    }
}
