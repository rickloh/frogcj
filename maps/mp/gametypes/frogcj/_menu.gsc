playerMenuListener()
{
    level endon("game_ended");
    self endon("disconnect");
    
    for(;;)
    {
        self waittill("menuresponse", menu, response);
        
        // Main CJ menu (i.e. save position, load position)
        if(menu == "-1") // We don't have an actual CJ scriptmenu as it is unnecessary
        {
            if(response == "save")
            {
                self notify("save_key_pressed");
            }
            else if(response == "load")
            {
                self notify("load_key_pressed");
            }
            else
            {
                // Unknown CJ menu response, ignore
            }
        }
        else if(menu == game["menu_frogcjoptions"])
        {
            if(response == "fullbright")
            {
                self.frogcj["fullbright"] = !self.frogcj["fullbright"];
                
                if(self.frogcj["fullbright"])
                {
                    self iprintln("Fullbright ^2enabled");
                    self setClientDvar("r_fullbright", 1);
                }
                else
                {
                    self iprintln("Fullbright ^1disabled");
                    self setClientDvar("r_fullbright", 0);
                }
            }
            else if(response == "sprintbobbing")
            {
                self.frogcj["sprintbobbing"] = !self.frogcj["sprintbobbing"];
                
                // Not client DVAR, but this version of mod is for devmap
                if(self.frogcj["sprintbobbing"])
                {
                    self iprintln("Sprint bobbing ^2enabled");
                    setDvar("player_sprintcamerabob", 0.5);
                }
                else
                {
                    self iprintln("Sprint bobbing ^1disabled");
                    setDvar("player_sprintcamerabob", 0);
                }
            }
            else if(response == "speedometer")
            {
                self.frogcj["speedometer"] = !self.frogcj["speedometer"];
                
                if(self.frogcj["speedometer"])
                    self.speedometerhud.alpha = 1;
                else
                    self.speedometerhud.alpha = 0;
            }
            else if(response == "rpgweapswitch")
            {
                self.frogcj["rpgweapswitch"] = !self.frogcj["rpgweapswitch"];
                
                if(self.frogcj["rpgweapswitch"])
                {
                    self iprintln("Auto RPG weapon switch ^2enabled");
                    self thread maps\mp\gametypes\frogcj\_player::performRPGWeapSwitch();
                }
                else
                {
                    self iprintln("Auto RPG weapon switch ^1disabled");
                    self notify("rpg_weap_switch_disabled");
                }
            }
            else
            {
                // Unknown options menu response
            }
        }
        else
        {
            // Unknown menu, ignore
        }
        
        waittillframeend; // At least have some sort of failsafe delay
    }
}