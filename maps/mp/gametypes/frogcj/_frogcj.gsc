init()
{
    game["menu_frogcjoptions"] = "frogcjoptions";
    game["menu_clientcmd"] = "clientcmd";
    
    precacheMenu(game["menu_frogcjoptions"]); // Quick options menu (B default key)
    precacheMenu(game["menu_clientcmd"]); // Used to set binds
    
    precacheString(&"FROGCJ_MOD_OPTIONS");
    precacheString(&"FROGCJ_1_OPTIONS");
    precacheString(&"FROGCJ_1_FULLBRIGHT");
    precacheString(&"FROGCJ_2_SPRINTBOBBING");
    precacheString(&"FROGCJ_3_SPEEDOMETER");
    precacheString(&"FROGCJ_4_RPGWEAPSWITCH");
    precacheString(&"FROGCJ_SAVE_POS");
    precacheString(&"FROGCJ_LOAD_POS");
    precacheString(&"FROGCJ_IN_AIR");
    precacheString(&"FROGCJ_NO_SAVES");
    precacheString(&"FROGCJ_NOCLIP_ENABLED");
    precacheString(&"FROGCJ_NOCLIP_DISABLED");
    
    precacheShader("arrow_up");
    precacheShader("arrow_down");
    precacheShader("arrow_left");
    precacheShader("arrow_right");
    
    precacheItem("rpg_mp");
    precacheItem("deserteaglegold_mp");
    
    precacheModel("body_mp_usmc_assault");
    precacheModel("head_mp_usmc_tactical_mich");
    precacheModel("viewmodel_base_viewhands");
}

setServerDvars()
{
    // Convenience DVARs
	setDvar("jump_slowdownenable", 0);
	setDvar("bg_fallDamageMaxHeight", 999999);
	setDvar("bg_fallDamageMinHeight", 999998);
    setDvar("fx_enable", 0);
    setDvar("r_fog", 0);
    
    // Codjumper QOL DVARs
    setDvar("player_sprinttime", 12.8);
    
    // Anti-cheat DVARs
    setDvar("g_gravity", 800);
    setDvar("g_speed", 190);
    setDvar("sv_fps", 20);
    setDvar("friction", 5.5);
    setDvar("jump_height", 39);
    setDvar("jump_stepsize", 18);
    setDvar("player_backspeedscale", 0.7);
    setDvar("player_sprintspeedscale", 1.5);
    setDvar("player_sprintstrafespeed", 0.667);
    setDvar("player_strafespeedscale", 0.8);
    setDvar("stopspeed", 100);
    
    // Other DVARs
	setDvar("scr_game_spectatetype", 2);
    setDvar("ui_hud_hardcore", 1);
}

// Called when a player connects (threaded)
setupConnectedPlayer()
{
    // Reset a couple client DVARs because we can't check their values (they can be toggled via options menu)
    self setClientDvar("r_fullbright", 0);
    // This is not a client DVAR, but this mod version is for devmap
    setDvar("player_sprintcamerabob", 0.5); 
    
    self.frogcj = [];
    self.frogcj["save"] = [];
    self.frogcj["save"]["timestamp"] = 0;
    
    self.frogcj["noclip"] = undefined;
    
    self.frogcj["fullbright"] = false;
    self.frogcj["rpgweapswitch"] = false; // Not implemented yet
    self.frogcj["sprintbobbing"] = true; // Sprint bobbing is on by default ingame as well
    self.frogcj["speedometer"] = true; // On by default - debatable, get user feedback
    
    self.frogcj["has_spawned"] = false;
    
    // Start some other background thread(s)
    self thread maps\mp\gametypes\frogcj\_player::savePositionListener();
    self thread maps\mp\gametypes\frogcj\_player::loadPositionListener();
    self thread maps\mp\gametypes\frogcj\_player::checkForNoclipKeys();
    self thread maps\mp\gametypes\frogcj\_menu::playerMenuListener();
}

initMovementHUD(name, dx, dy, shaderName)
{
    if (!isDefined(self.hud[name]))
    {
        self.hud[name] = newClientHudElem(self);
        self.hud[name].horzAlign = "center";
        self.hud[name].vertAlign = "bottom";
        self.hud[name].alignX = "center";
        self.hud[name].alignY = "bottom";
        self.hud[name].x = self.arrowHudCenterPos[0] + dx;
        self.hud[name].y = self.arrowHudCenterPos[1] + dy;
        self.hud[name].foreground = true;
        self.hud[name].hideWhenInMenu = true;
        self.hud[name] setShader(shaderName, self.arrowHudSize[0], self.arrowHudSize[1]);
        self.hud[name].alpha = 0;
    }
}

initHUD(name, hAlign, vAlign, x, y, foreground, font, hideInMenu, color, glowColor, glowAlpha, fontScale, archived, alpha)
{
    self.hud[name] = newClientHudElem(self);
    self.hud[name].horzAlign = hAlign;
    self.hud[name].vertAlign = vAlign;
    self.hud[name].alignX = hAlign;
    self.hud[name].alignY = vAlign;
    self.hud[name].x = x;
    self.hud[name].y = y;
    self.hud[name].foreground = foreground;
    self.hud[name].font = font;
    self.hud[name].hideWhenInMenu = hideInMenu;
    self.hud[name].color = color;
    self.hud[name].glowColor = glowColor;
    self.hud[name].glowAlpha = glowAlpha;
    self.hud[name].fontScale = fontScale;
    self.hud[name].archived = archived;
    self.hud[name].alpha = alpha;
}

setupHuds()
{
    // Position in center in between all movement arrows, used as reference
    self.arrowHudCenterPos = [];
    self.arrowHudCenterPos[0] = 0;
    self.arrowHudCenterPos[1] = -30;
    
    // Size of arrow images to display
    self.arrowHudSize = [];
    self.arrowHudSize[0] = 20;
    self.arrowHudSize[1] = 20;
    
    // Offset used to bring the movement HUD together
    movHudOffset = [];
    movHudOffset[0] = 16;
    movHudOffset[1] = 16;
    
    self.hud = [];
    
    // Draw movement HUD
    self initMovementHUD("w",       0,                      -1 * movHudOffset[1], "arrow_up"    );
    self initMovementHUD("a",      -1 * movHudOffset[0],     0,                   "arrow_left"  );
    self initMovementHUD("s",       0,                       1 * movHudOffset[1], "arrow_down"  );
    self initMovementHUD("d",       1 * movHudOffset[0],     0,                   "arrow_right" );
    
    // Draw other HUDS
    self initHUD("stance", "center", "bottom",  1 * movHudOffset[0],  -1 * movHudOffset[1], true, "objective", true, (1.0, 1.0, 1.0), (0.0, 0.0, 0.0), 0.0, 1.4, false, 1);
    
    self initHUD("space",  "center", "bottom", -1 * movHudOffset[0],  -1 * movHudOffset[1], true, "objective", true, (1.0, 1.0, 1.0), (0.0, 0.0, 0.0), 0.0, 1.4, false, 0);
    self.hud["space"] setText("J");
    
    self initHUD("use",    "center", "bottom", -1 * movHudOffset[0],  -3 * movHudOffset[1], true, "objective", true, (1.0, 1.0, 1.0), (0.0, 0.0, 0.0), 0.0, 1.4, false, 0);
    self.hud["use"] setText("U");
    
    // Draw FPS HUD
    self initHUD("fps", "right", "bottom", -20, -15, true, "objective", true, (1.0, 1.0, 1.0), ((20/255), (33/255), (125/255)), 0.0, 2, false, 1);
    
    // Draw speedometer HUD
    self initHUD("speed", "left", "bottom", 20, -15, true, "objective", true, (1.0, 1.0, 1.0), ((125/255), (33/255), (20/255)), 0.0, 2, false, 1);
}

setupButtons()
{
    // We use the 'code' to detect when the game improperly toggles the dvar (in that case it toggles from frametime <-> frametime or code <-> code instead of between code <-> frametime
    self.heldButtons = [];
    
    i = self.heldButtons.size;
    self.heldButtons[i] = [];
    self.heldButtons[i]["name"] = "w";
    self.heldButtons[i]["code"] = "119";
    
    i = self.heldButtons.size;
    self.heldButtons[i] = [];
    self.heldButtons[i]["name"] = "a";
    self.heldButtons[i]["code"] = "97";
    
    i = self.heldButtons.size;
    self.heldButtons[i] = [];
    self.heldButtons[i]["name"] = "s";
    self.heldButtons[i]["code"] = "115";
    
    i = self.heldButtons.size;
    self.heldButtons[i] = [];
    self.heldButtons[i]["name"] = "d";
    self.heldButtons[i]["code"] = "100";
    
    i = self.heldButtons.size;
    self.heldButtons[i] = [];
    self.heldButtons[i]["name"] = "space";
    self.heldButtons[i]["code"] = "32";
    
    
    self.pressedButtons = [];
    
    i = self.pressedButtons.size;
    self.pressedButtons[i]["name"] = "mwheelup";
    self.pressedButtons[i]["expiryMs"] = 250.0; // After how many ms the button should be marked as inactive upon press
}

// Called when a player spawns (threaded)
setupSpawnedPlayer()
{
    // Set any necessary cvars
    self setClientDvar("aim_automelee_range", 0);
    
    self detachAll();
    self setModel("body_mp_usmc_assault");
    self attach("head_mp_usmc_tactical_mich", "", true);
    self setViewmodel("viewmodel_base_viewhands");
    
    // Give the player their well earned weaponry
    self takeAllWeapons();
    mainWeapon = "deserteaglegold_mp";
    
    // Fix stowed weapons
    self maps\mp\gametypes\_weapons::detach_all_weapons();
    
    // Wait until weapons have actually been taken away
    wait .05;
    
    self giveWeapon(mainWeapon);
    
    self giveWeapon("rpg_mp");
	self setActionSlot(4, "weapon", "rpg_mp");
	self setWeaponAmmoClip("rpg_mp", 2);
	self setWeaponAmmoStock("rpg_mp", 9);
    
    // Wait until the player has a deagle before switching to it
    wait .05;
    
    if(self getCurrentWeapon() != mainWeapon)
    {
        // Switch to the deagle
        self switchToWeapon(mainWeapon);
    }
    
    // One-time initializations
    if(!self.frogcj["has_spawned"])
    {
        // Define spawn position as first saved position for now
        self.frogcj["save"]["pos"] = self getOrigin();
        self.frogcj["save"]["ang"] = self getPlayerAngles();
        
        self setupHuds();
        
        self setupButtons();
        
		self.frogcj["has_spawned"] = true;
	}
    
    self thread maps\mp\gametypes\frogcj\_player::mainPlayerLoop();
}